import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.time.temporal.ChronoUnit;
import java.time.Duration;

public class TimeUtil {

    public TimeUtil() {

    }

    public static TimeUtil builder() {
        return new TimeUtil();
    }

    public LocalDateTime plus(LocalDateTime dateTime, int minutes) {
        return dateTime.plusMinutes(minutes);
    }

    public LocalDateTime minus(LocalDateTime dateTime, int minutes) {

        return dateTime.minusMinutes(minutes);
    }

    public int minBetween(LocalDateTime v1, LocalDateTime v2)
    {
        long minutes = v1.until(v2, ChronoUnit.MINUTES);
        return (int) minutes;
    }
    /**
     *  초단위를 버리처리하고 분단위를 비교하여 v1 보다 v2가 시간이 크면 true한다.
     * @param v1 target시간
     * @param v2 비교시간
     * @return
     */
    public boolean isGreatThen(LocalDateTime v1, LocalDateTime v2) {
        return v1.truncatedTo(ChronoUnit.MINUTES).isBefore(v2.truncatedTo(ChronoUnit.MINUTES));
    }

    @Test
    public void compareTest()
    {
        LocalDateTime v1 = LocalDateTime.now();
        LocalDateTime v2 = v1.plusSeconds(10);
        LocalDateTime v3 = v1.plusMinutes(30).plusSeconds(10);

        System.out.println( v1 +":"+ v2  + " " +v3+ ":"+ isGreatThen(v1,v2)) ;

        System.out.println( v1 +"=========="+ v3 +":" + minBetween(v1,v3)) ;

    }
}
