import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionTest {
    enum Try {SUCCESS
        ,FAIL
        ,COMFROM
    }



    public static void main(String args[] )
    {
        BiFunction<Charge,Charge, EnumMap<Try,String>>  chkChargef= (frontCh,rearCh)->{
            int x =frontCh.getWidth() - rearCh.getWidth();
            EnumMap<Try,String>  result = new EnumMap<Try, String>(Try.class);
            if (x==0)
            {
                result.put(Try.SUCCESS,"성공");
            } else if(x<0){
                result.put(Try.FAIL,"실패".concat(frontCh.getPlanChargeNo()).concat(",").concat(rearCh.getPlanChargeNo()).concat(":").concat(String.valueOf(x)));

            } else
            {
                result.put(Try.COMFROM,"확인".concat(frontCh.getPlanChargeNo()).concat(",").concat(rearCh.getPlanChargeNo()).concat(":").concat(String.valueOf(x)));
            }
            return result;
        };







        List<String> test = Arrays.asList("A","E","C","D");
        Collections.sort(test,(a,b)->a.compareTo(b));
        System.out.println(test);
        FunctionTest ftest = new FunctionTest();
        List<Charge> chargeList = ftest.makeCharge();
        System.out.println("  결과 "+ftest.betweenCheck(chargeList,chkChargef));
    }

    public boolean betweenCheck( List<Charge> chargeList, BiFunction<Charge,Charge, EnumMap<Try,String>> f)
    {
        List<EnumMap<Try, String>> message = new ArrayList<>();
        EnumMap<Try, String> val = null;
        for (int i=0,size=chargeList.size()-1;i<size;i++)
        {
            val= f.apply(chargeList.get(i),chargeList.get(i+1));
            message.add(val);

        }
        message.stream().filter(x->(x.containsKey(Try.FAIL))  )
                .map(x->x.get(Try.FAIL))
                .forEach(str->System.out.println(str));
        return message.stream().allMatch(x->x.containsKey(Try.SUCCESS));
        // message.stream().filter( x -> x.containsKey(Try.FAIL)==true)
    }

    public  List<Charge> makeCharge()
    {
        List<Charge> chargeList = new ArrayList<>();
        chargeList.add(new Charge("AAA1",1200));
        chargeList.add(new Charge("AAA2",1100));
        chargeList.add(new Charge("AAA3",1100));
        chargeList.add(new Charge("AAA4",1010));
        chargeList.add(new Charge("AAA5",1210));
        return chargeList;
    }

    public class  Charge {
        private  String planChargeNo =null;
        private  int width =0;
        private  String message;
        public Charge(String planChargeNo, int width) {
            this.planChargeNo = planChargeNo;
            this.width = width;
        }

        public String getPlanChargeNo() {
            return planChargeNo;
        }

        public void setPlanChargeNo(String planChargeNo) {
            this.planChargeNo = planChargeNo;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }
        public void setMessage(String message)
        {
            this.message =message;
        }

        public String getMessage() { return this.message;}
    }
}
