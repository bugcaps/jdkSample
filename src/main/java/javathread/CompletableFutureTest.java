package javathread;


import org.junit.Test;

import java.util.concurrent.CompletableFuture;


/**
 * CompletableFuture의 thenAccept,
 *  thenAcceptSync 를 비교해 보면 thenAcceptSync method는 asyncPool 을 가져와서 사용한다.
 *asyncPool 은 아래와 같이 parallelism 을 지원한다면 ForkJoinPool, 아니면 ThreadPerTaskExecutor 를 사용한다.
 */
public class CompletableFutureTest {


    // 비동기처리 Return 값을 다음 처리의 Parameter 로 사용할때 사용한다 
    @Test
    public void thenComposeTest() throws Exception {
        Price price = new Price();
        price.getPriceAsync(1)
                .thenComposeAsync(price::getPriceAsync)
                .thenComposeAsync(price::getPriceAsync)
                .thenComposeAsync(r -> price.getPriceAsync(r)).join();

        System.out.println("Non Blocking!!");

        // main thread 가 죽으면 child 도 다 죽어 버려서 대기함. 
        Thread.sleep(5000l);

    }


    static class Price {

        public double getPrice(double oldprice) throws Exception {
            return calculatePrice(oldprice);
        }


        public double calculatePrice(double oldprice) throws Exception {

            System.out.println("Input :" + oldprice);
            Thread.sleep(1000l);
            System.out.println("Output :" + (oldprice + 1l));
            return oldprice + 1l;


        }


        public CompletableFuture<Double> getPriceAsync(double oldPrice) {
            CompletableFuture<Double> completableFuture = new CompletableFuture<>();
            new Thread(() -> {
                try {
                    double price = calculatePrice(oldPrice);
                    completableFuture.complete(price);
                } catch (Exception ex) {
                    completableFuture.completeExceptionally(ex);
                }
            }).start();

            return completableFuture;
        }
    }
}


/**thenCompose
 * CompletableFuture 를 반환하는 Method를 Chain으로 실행하고 싶을때..
*즉 이전에 Async 프로세스로 응답 받은 값을 다음 Async 프로세스의 인자로 사용하는 경우에 아래와 같이 thenCompose, thenComposeAsync 를 사용할 수 있다
**/