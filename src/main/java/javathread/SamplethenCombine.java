package javathread;

import java.util.concurrent.CompletableFuture;

public class SamplethenCombine {



    public static void main(String[] args) {
        final CompletableFuture<Integer> task1 = CompletableFuture.supplyAsync(() ->{
            System.out.println("task1 start");
            try {
                Thread.sleep(3000l);
            } catch (Exception e) { e.printStackTrace();}
            System.out.println("task1 end");
            return 2;
        } );

        final CompletableFuture<Integer> task2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("task2 start");
            try {
                Thread.sleep(1000l);
            } catch (Exception e) {e.printStackTrace();}
            System.out.println("task2 end");
            return 4;
        } );

        final CompletableFuture<Integer> task3 = CompletableFuture.supplyAsync(() -> {
            System.out.println("task3 start");
            try {
                Thread.sleep(500l);
            } catch (Exception e) {e.printStackTrace();}
            System.out.println("task3 end");
            return 4;
        } );

        //3개의 thread가 모두 실행완료되면 결과값을 받아서 실행한다.
        task1.thenCombine(task2, (value1, value2) -> {
            System.out.println("task2 combine");
                 return value1 + value2;
                }
           ).thenCombine(task3,(value1,value2) -> {
            System.out.println("task3 combine");
            return value1 + value2;
        })
                .thenAccept(System.out::println);
        System.out.println("task2 11");
        task1.complete(100);
    try {
        Thread.sleep(3000l);
    } catch (Exception e) { e.printStackTrace();}
        System.out.println("task2 22");
    }
}
