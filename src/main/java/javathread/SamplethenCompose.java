package javathread;

import java.util.concurrent.CompletableFuture;

public class SamplethenCompose {
    public static CompletableFuture<Integer> createTask(int input) {
        return CompletableFuture.supplyAsync(() -> input + 1);
    }

    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> 2)  // 1번. thread로 실행 2를 return 한다.
                .thenCompose(input -> createTask(input))  //2번: 1번실행이완료되면 2번을 실행한다.
                .thenAccept(System.out::println);       // 2번의 실행결과를 return 값만 출력한다.
    }
}
