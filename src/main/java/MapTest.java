

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;

public class MapTest
{
    public Map<Integer, String> sample()
    {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "value 1");
        map.put(2, "value 2");
        map.put(3, "value 3");
        map.put(4, "value 4");
        map.put(5, "value 5");
        return map;
    }
    @Test
    public void mapRemove()
    {

        Map<Integer, String> map = sample();
// 이렇게 iterator에서  	ConcurrentModificationException 발생
//		for(Iterator<Integer> iterator = map.keySet().iterator(); iterator.hasNext(); ) {
//			  Integer key = iterator.next();
//			  if(key != 1) {
//			    iterator.remove();
//			  }
//			}
        //java8에서는

        map.values().removeIf(value->value.contains("2"));
        System.out.println(map);
        map.keySet().removeIf(key->key!=1);



    }

    @Test
    public void testMapIteration() {
        List<String> langs = new ArrayList<>();

        Map<Integer, String> map = sample();
        map.forEach((k, v) -> langs.add(v));
        assert langs.size() == 5;
    }

    public  Map<String, Integer>  getMapSortData() {
        final Map<String, Integer> wordCounts = new HashMap<>();
        wordCounts.put("USA", 100);
        wordCounts.put("jobs", 200);
        wordCounts.put("software", 50);
        wordCounts.put("technology", 70);
        wordCounts.put("opportunity", 200);
        // putIfAbsent key에 값이 null일경우 값을 설정하고 값이 있을경우 설정하지 않는다.
        wordCounts.putIfAbsent("test",30);
        wordCounts.putIfAbsent("test",50);
        wordCounts.putIfAbsent("test2",null);
        wordCounts.putIfAbsent("test2",35);
        return wordCounts;

    }
    @Test
    public void mapSort()
    {
        Map<String, Integer> wordCounts = getMapSortData();
        Map<String,Integer> sortedByCount = wordCounts.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue,
                        (e1,e2)->e1,LinkedHashMap::new));
        System.out.println(sortedByCount);

    }
}
