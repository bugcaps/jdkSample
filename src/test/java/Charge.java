import java.time.LocalDateTime;

public class Charge {
    private LocalDateTime startDt=null;
    private LocalDateTime endDt=null;

    public LocalDateTime getStartDt() {
        return startDt;
    }

    public void setStartDt(LocalDateTime startDt) {
        this.startDt = startDt;
    }

    public LocalDateTime getEndDt() {
        return endDt;
    }

    public void setEndDt(LocalDateTime endDt) {
        this.endDt = endDt;
    }

    public String getMtlNo() {
        return mtlNo;
    }

    public void setMtlNo(String mtlNo) {
        this.mtlNo = mtlNo;
    }

    private String mtlNo=null;
    public  Charge( String mtlNo)
    {
        this.mtlNo = mtlNo;
    }
}
