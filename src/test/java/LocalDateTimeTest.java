import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LocalDateTimeTest
{

    public static void main(String[] args) {
        List<Charge> list =createDate();

        list.forEach(x-> {
             x.setStartDt(x.getStartDt().plusMinutes(10));
            System.out.println(x.getStartDt());
        });
    }


    public static  List<Charge> createDate()
    {
        List<Charge> list = new ArrayList<>();

        Charge a = new Charge("1");
        a.setStartDt(LocalDateTime.of(2019,2,1,10,30,20));
        a.setEndDt(LocalDateTime.of(2019,2,1,11,10,32));
        list.add( a);
        Charge b = new Charge("2");
        b.setStartDt(LocalDateTime.of(2019,2,1,11,15,20));
        b.setEndDt(LocalDateTime.of(2019,2,1,11,40,32));
        list.add(  b     );
        return list;

    }

    public static LocalDateTime createDt(int year, int month, int dayOfMonth, int hour, int minute, int second) {
        return LocalDateTime.of(year,month,dayOfMonth,hour,minute,second);
    }


}
